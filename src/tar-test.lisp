(defpackage tar/test
  (:local-nicknames (:put :tar)
                    (:a :alexandria))
  (:use :cl :parachute))

(in-package :tar/test)

(defmacro with-input-as-octets ((var string) &body body)
  (a:with-gensyms (buffer vector)
    (a:once-only (string)
      `(let* ((,buffer (loop :with ,vector := (make-array (list (length ,string))
                                                          :element-type '(unsigned-byte 8))
                             :for char :across ,string
                             :for position :from 0
                             :do (setf (aref ,vector position) (char-code char))
                             :finally (return ,vector)))
              (,var (trivial-octet-streams:make-octet-input-stream ,buffer)))
         ,@body))))

(define-test read-null-terminated-string
  :serial nil

  (loop :for (input size expected)
          :in '(("hello     " 10 "hello")
                ("      " 6 ""))
        :do (with-input-as-octets (stream input)
              (is string= expected (put::read-null-terminated-string stream size)))))


(define-test write-null-padded-string
  :serial nil

  )
