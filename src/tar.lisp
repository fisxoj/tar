(defpackage tar
  (:use :cl)
  (:local-nicknames (:log :beaver))
  (:export
   #:extract
   #:archive))

(in-package :tar)
;; start width (bytes)	field
;; 0 	 100	 	File name
;; 100 	 8	 	File mode (octal)
;; 108 	 8	 	Owner's numeric user ID (octal)
;; 116 	 8	 	Group's numeric user ID (octal)
;; 124 	 12	 	File size in bytes (octal)
;; 136 	 12	 	Last modification time in numeric Unix time format (octal)
;; 148 	 8	 	Checksum for header record
;; 156 	 1	 	Type flag
;; 157 	 100	 	Linked filename
;; 257 	 6	 	UStar indicator "ustar" then NUL
;; 263 	 2	 	UStar version "00"
;; 265 	 32	 	Owner user name
;; 297 	 32	 	Owner group name
;; 329 	 8	 	Device major number
;; 337 	 8	 	Device minor number
;; 345 	 155	 	Filename prefix

(defparameter +ustar-indicator+ "ustar  ")

(eval-when (:load-toplevel)
  (macrolet ((type-flag (name value)
               `(defconstant ,(intern (format nil "+TYPE-FLAG-~:@(~A~)+" name)) ,value)))
    (type-flag normal-file #\0)
    (type-flag directory #\5)
    (type-flag hard-link #\1)
    (type-flag symbolic-link #\2)))

;; (defparameter +ustar-version+ "00")

(defstruct (header (:constructor %make-header))
  (filename "" :type string)
  (mode "" :type string)
  (owner-id "" :type string)
  (group-id "" :type string)
  (size 0 :type integer)
  (last-modified 0 :type integer)
  (checksum "" :type string)
  (type-flag #\Null :type base-char)
  (linked-filename "" :type string)
  (owner-username "" :type string)
  (owner-group-name "" :type string)
  (device-major-number "" :type string)
  (device-minor-number "" :type string)
  (filename-prefix "" :type string))


(defun header-size-string (header)
  (declare (type header header))
  (format nil "~8,11,'0R" (header-size header)))


(defun header-last-modified-string (header)
  (declare (type header header))
  (format nil "~8,11,'0R" (header-last-modified header)))


(defun header-type-flag-string (header)
  (declare (type header header))
  (make-string 1 :initial-element (header-type-flag header)))


(defun empty-header-p (header)
  (declare (type header header))
  (and (string= (header-filename header) "")
       (string= (header-mode header) "")
       (string= (header-owner-id header) "")
       (string= (header-group-id header) "")
       (= (header-size header) 0)))


(defun header-directory-p (header)
  (char= (header-type-flag header)
         +type-flag-directory+))


(defun header-normal-file-p (header)
  (char= (header-type-flag header)
         +type-flag-normal-file+))


(defun header-calculate-checksum (header)
  "Sum the byte values in the header into a positive integer and return that value and also the padded octal string representation.

Calculates the value as though the 8 bytes of the checksum field were spaces, as suggested in the `Wikipedia page <https://en.wikipedia.org/wiki/Tar_(computing)#Header>`_."
  (let ((sum 0))
    (flet ((add-string (string)
             (incf sum (loop :for char :across string
                             :summing (char-code char)))))
      (add-string (header-filename header))
      (add-string (header-mode header))
      (add-string (header-owner-id header))
      (add-string (header-group-id header))
      (add-string (header-size-string header))
      (add-string (header-last-modified-string header))
      (add-string (header-type-flag-string header))
      (add-string (header-linked-filename header))
      (add-string +ustar-indicator+)
      (add-string (header-owner-username header))
      (add-string (header-owner-group-name header))
      (add-string (header-device-major-number header))
      (add-string (header-device-minor-number header))
      (add-string (header-filename-prefix header))
      ;; Empty checksum string
      (incf sum (* 8 (char-code #\space))))
    (values sum (format nil "~8,7,'0R" sum))))


(define-condition decode-error ()
  ())


(defun read-null-terminated-string (stream size)
  (loop :with string := (make-string size)
        :for position :of-type fixnum :from 0 :below size
        :for byte := (read-byte stream)
        :until (zerop byte)
        :do (setf (char string position) (code-char byte))
        :finally (progn
                   (loop :repeat (- size position 1) :do (read-byte stream))
                   (return (subseq string 0 position)))))


(defun write-null-padded-string (stream size string)
  (loop :with string-length := (length string)
        :for i :below size
        :do (write-byte
             (cond
               ((< i string-length)
                (char-code (char string i)))
               ;; null-padding
               ((>= i string-length) 0))
             stream)))


(defun make-header-from-byte-stream (stream)
  (declare (optimize space speed))
  (flet ((read-string (size)
           (read-null-terminated-string stream size))
         (read-integer (size)
           ;; junk-allowed is true to allow empty strings, and the 'or'
           ;; converts them to 0s.
           (or (parse-integer (read-null-terminated-string stream size)
                              :radix 8
                              :junk-allowed t)
               0)))
    (let ((filename            (read-string 100))
          (mode                (read-string 8))
          (owner-id            (read-string 8))
          (group-id            (read-string 8))
          (size                (read-integer 12))
          (last-modified       (read-integer 12))
          (checksum            (read-string 8))
          (type-flag           (char (the simple-string (read-string 1)) 0))
          (linked-filename     (read-string 100))
          (ustar               (read-string 6))
          (ustar-version       (read-string 2))
          (owner-username      (read-string 32))
          (owner-group-name    (read-string 32))
          (device-major-number (read-string 8))
          (device-minor-number (read-string 8))
          (filename-prefix     (read-string 155)))
      (declare (ignore ustar ustar-version))
      ;; read 12 bytes of padding, up to 512
      (loop :repeat 12 :do (read-byte stream))
      ;; TODO: It seems like this sentinel might be different in
      ;; different archive formats.

      ;; (assert (and (not (alexandria:emptyp ustar))
      ;;              (string= (subseq ustar 0 5)
      ;;                       +ustar-indicator+))
      ;;         nil
      ;;         'decode-error)
      ;; (assert (string= ustar-version +ustar-version+) nil 'decode-error)
      (%make-header :filename filename
                    :mode mode
                    :owner-id owner-id
                    :group-id group-id
                    :size size
                    :last-modified last-modified
                    :checksum checksum
                    :type-flag type-flag
                    :linked-filename linked-filename
                    :owner-username owner-username
                    :owner-group-name owner-group-name
                    :device-major-number device-major-number
                    :device-minor-number device-minor-number
                    :filename-prefix filename-prefix))))


(defun copy-stream-to-stream (input output &key element-type buffer-size size)
  "Copy the contents of the INPUT stream into the OUTPUT stream.
If LINEWISE is true, then read and copy the stream line by line, with an optional PREFIX.
Otherwise, using WRITE-SEQUENCE using a buffer of size BUFFER-SIZE."
  (loop
    :with buffer-size := (or buffer-size 8192)
    :with buffer := (make-array (list buffer-size)
                                :element-type (or element-type 'character))
    :for total-size := 0 :then (+ total-size end)
    :for end := (read-sequence buffer input :end (min buffer-size (- size total-size)))
    :until (zerop end)
    :do (write-sequence buffer output :end end)
        (when (< end buffer-size) (return))))


(defun extract (pathname-or-stream output-directory &key (buffer-size 4096))
  (ensure-directories-exist output-directory)
  (let ((output-directory-pathname (pathname output-directory)))
    (labels ((extract-file (stream)
               (let* ((header (make-header-from-byte-stream stream))
                      (output-pathname (merge-pathnames (pathname (header-filename header))
                                                        output-directory-pathname)))
                 ;; If we've gotten an empty header (likely 512
                 ;; nulls), assert the next 512 are also null and
                 ;; we're done with the file!
                 (when (empty-header-p header)
                   (let* ((trailer (make-array (list 512)
                                               :element-type '(unsigned-byte 8))))
                     (when (= 512 (read-sequence trailer stream))
                       (loop :for value :across trailer
                             :unless (zerop value)
                               :do (error 'decode-error))
                       (return-from extract))))
                 (alexandria:eswitch ((header-type-flag header)
                                      :test 'char=)
                   ((or +type-flag-normal-file+ #\Null)
                    (let ((padding-bytes (- 512 (rem (header-size header) 512))))
                      (with-open-file (out output-pathname
                                           :direction :output
                                           :if-does-not-exist :create
                                           :if-exists :error
                                           :element-type '(unsigned-byte 8))
                        (log:debug
                         "Extracting ~d bytes of ~s."
                         (header-size header)
                         output-pathname)
                        (copy-stream-to-stream stream out
                                               :element-type '(unsigned-byte 8)
                                               :buffer-size buffer-size
                                               :size (header-size header)))
                      (loop :repeat padding-bytes :do (read-byte stream))))
                   (+type-flag-directory+
                    ;; directory
                    (ensure-directories-exist output-pathname))
                   (+type-flag-symbolic-link+
                    (osicat:make-link (header-filename header)
                                      :target (header-linked-filename header))))))
             (extract (stream)
               (handler-case
                   (loop :do (extract-file stream))
                 (end-of-file () nil))))
      (etypecase pathname-or-stream
        (stream
         (extract pathname-or-stream))
        ((or pathname string)
         (with-open-file (in pathname-or-stream
                             :element-type '(unsigned-byte 8))
           (extract in)))))))


(defun relative-pathname (root-dir pathname)
  (assert (uiop:directory-pathname-p root-dir) (root-dir)
          "ROOT-DIR must be a directory, got ~S." root-dir)
  ;; note: TRUENAME resolves things like paths relative to :HOME into
  ;; a canonical form
  (let* ((root-dir-directory (pathname-directory (truename root-dir)))
         (pathname-directory (pathname-directory (truename pathname)))
         (pathname-difference (nreverse (set-difference pathname-directory root-dir-directory :test #'string=))))
    (make-pathname :directory `(:relative ,@pathname-difference)
                   :defaults pathname)))


(defun make-header-for-file (root-dir pathname)
  (declare (optimize debug))
  (let* ((file-kind (osicat:file-kind pathname))
         (stat (osicat-posix:stat pathname))
         (header
           (%make-header :filename (namestring (relative-pathname root-dir pathname))
                         ;; TODO: Look into why we only want the lower bytes of mode
                         :mode (format nil "~8,7,'0R" (logand (osicat-posix:stat-mode stat) #xfff))
                         :owner-id (format nil "~8,7,'0R" (osicat-posix:stat-uid stat))
                         :group-id (format nil "~8,7,'0R" (osicat-posix:stat-gid stat))
                         :size (if (eq file-kind :regular-file) (osicat-posix:stat-size stat) 0)
                         :last-modified (osicat-posix:stat-mtime-sec stat)
                         :checksum ""
                         :type-flag (ecase file-kind
                                      (:regular-file +type-flag-normal-file+)
                                      (:directory +type-flag-directory+)
                                      (:symbolic-link +type-flag-symbolic-link+))
                         :linked-filename (if (eq file-kind :symbolic-link)
                                              (osicat:read-link pathname)
                                              "")
                         :owner-username (cdr (assoc :name (osicat:user-info (osicat-posix:stat-uid stat))))
                         :owner-group-name ""
                         :device-major-number ""
                         :device-minor-number ""
                         :filename-prefix "")))

    (setf (header-checksum header) (nth-value 1 (header-calculate-checksum header)))
    (values header)))


(defun write-header (stream header)
  (check-type header header)
  (macrolet ((w (field size)
               `(write-null-padded-string stream
                                          ,size
                                          (,(intern (format nil "HEADER-~a" field)) header))))
    (w filename 100)
    (w mode 8)
    (w owner-id 8)
    (w group-id 8)
    (write-null-padded-string stream 12 (header-size-string header))
    (write-null-padded-string stream 12 (header-last-modified-string header))
    (w checksum 8)
    (write-null-padded-string stream 1 (header-type-flag-string header))
    (w linked-filename 100)
    (write-null-padded-string stream 8 +ustar-indicator+)
    ;; (write-null-padded-string stream 2 "00")
    (w owner-username 32)
    (w owner-group-name 32)
    (w device-major-number 8)
    (w device-minor-number 8)
    (w filename-prefix 155))
  ;; pad to 512 bytes
  (write-sequence (make-array '(12)
                              :initial-element 0
                              :element-type '(unsigned-byte 8))
                  stream))


(defun write-file-chunked (stream pathname)
  "Writes a file to STREAM in chunks of 512 bytes, padding the end with null bytes if necessary to reach a multiple of 512 bytes."
  (with-open-file (input pathname :direction :input
                                  :element-type '(unsigned-byte 8))
    (loop :with buffer := (make-array '(512) :element-type '(unsigned-byte 8))
          :for bytes-read := (read-sequence buffer input)
          :do (log:debug "Writing ~D bytes to file." bytes-read)
          :do (write-sequence buffer stream :end bytes-read)
          :until (< bytes-read 512)
          ;; pad out the last block to 512 bytes
          :finally (write-sequence (make-array (list (- 512 bytes-read))
                                               :initial-element 0
                                               :element-type '(unsigned-byte 8))
                                   stream))))


(defun collect-files (root-directory)
  "Collects files under ROOT-DIRECTORY and makes a list of them."
  (let (files)
    (uiop:collect-sub*directories root-directory
                                  (constantly t)
                                  (constantly t)
                                  (lambda (p) (push p files)))
    (remove-duplicates
     ;; reversing here ensures directories appear before the files
     ;; they contain in the archive and thus the decoding program
     ;; doesn't need to implicitly create directories before their
     ;; entry in the archive shows up.
     (nreverse
      (mapcan (lambda (dir)
                (directory (make-pathname :defaults dir :name :wild :type :wild :version :wild)))
              files)))))


(defun archive (root-directory output-pathname &key (if-exists :supersede) (if-does-not-exist :create))
  (with-open-file (output output-pathname :direction :output
                                          :element-type '(unsigned-byte 8)
                                          :if-exists if-exists
                                          :if-does-not-exist if-does-not-exist)

    (loop :for pathname :in (collect-files root-directory)
          :for header := (make-header-for-file root-directory pathname)
          :do (log:with-fields (:pathname pathname string)
                (log:debug "Adding file to archive." pathname))
          :do (write-header output header)
          :do (when (header-normal-file-p header)
                (write-file-chunked output pathname)))))
