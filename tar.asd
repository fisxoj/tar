(defsystem tar
  :depends-on ("alexandria"
               "osicat")
  :pathname "src/"
  :components ((:file "tar")))

(defsystem tar/test
  :depends-on ("tar"
               "parachute"
               "alexandria"))
